package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsfHibernateExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsfHibernateExampleApplication.class, args);
	}

}
